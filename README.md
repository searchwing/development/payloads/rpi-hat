# rpi-sht-hat

This PCB is a raspberry pi HAT (Hardware attached on top). It is designed to replace a special cable, which includes the following features:

Humidity Sensor
Power Supply to Raspberry Pi
Communication between Raspberry Pi and Pixracer
The previous used sensor DHT22 is replaced by a SHT31, a digital sensor for humidity and temperature. It communicates with the raspberry pi via I2C. Power is supplied by the stackable power board via the JST connector. Communication with the autopilot is also done over the JST connector. Connection to the raspberry pi is established via a 2.54mm pitch female SMD socket.

![image](Documents/images/rpi-sht-hat-3d.png)

## Wiki

Also documented in the [SearchWing wiki](https://wiki.searchwing.org/en/home/Development/Electronics/rpi-sht-hat).

## Read Measurements from Pi

To read the sensor we use the device tree to tell te linux kernel where and how to speak with the sensor:

```
/*
 * Device tree overlay for sht3x sensor on i2c-6 (pins: 22,23 i2c-address: 0x44)
 * author: strg-v
 * date: 2023-01-18
 * Compile:
 * sudo dtc -@ -I dts -O dtb -o /boot/overlays/searchwing_sht3x.dtbo searchwing_sht3x_overlay.dts
 */

/dts-v1/;
/plugin/;

// sht3x Sensor
/{
    compatible = "brcm,bcm2711";

    // enable sensor I2C
    fragment@0 {
      target = <&i2c6>;
      __overlay__ {
         pinctrl-0 = <&i2c6_pins>;
         status = "okay";
         pinctrl-names = "default";
         clock-frequency = <100000>;
      };
    };

    // pins for i2c6 bus
    fragment@1 {
      target = <&gpio>;
      __overlay__ {
         i2c6_pins: i2c6 {
            brcm,pins = <22 23>;
         };
      };
    };

    // description of sht3x sensor
    fragment@2 {
        target = <&i2c6>;  // 7e205c00
        __overlay__ {
            #address-cells = <1>;
            #size-cells = <0>;
            sht3x: sht3x@44 {
				compatible = "sht3x";
				reg = <0x44>;
				status = "okay";
            };
        };
       
   };

};
```

In order to use this device tree source file (saved to searchwing_sht3x_overlay.dts), it has to be compiled by this command:

`sudo dtc -@ -I dts -O dtb -o /boot/overlays/searchwing_sht3x.dtbo searchwing_sht3x_overlay.dts`

By adding the following line to `/boot/config.txt`, the device tree overlay will be loaded during boot:

```
dtoverlay=searchwing_sht3x
```
With the package `lm-sensors` (install with `sudo apt install lm-sensors`) you can read humidity and temperature from the terminal.

If you want to read the temperature or humidity with e.g. python, you can read the following files:

`/sys/class/hwmon/hwmon2/humidity1_input`
and
`/sys/class/hwmon/hwmon2/temp1_input`